# Test for Kaplan 
**Plugin for video list provideres**

* Provider used
 * youtube
 
# Task
 **Topic**: Render Video Gallery from Youtube

 **ENDPOINT**: https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&order=date&playlistId=PLdYcdVQDHNRK0eyAwBDm8EiyLrnStj0Ae&key=AIzaSyAhDYYnDwphzM-DjwpvwYg43KEX39TBKAQ
 
 **Extra Parameter**: &maxResults=””
 
 **Description**:
 
 * The candidate has to create a basic WordPress theme and display the results of a YouTube playlist on a page.
 * In order to achieve this they will need to fetch the feed from the given YouTube endpoint and display the results on the front page of the site.
 * The candidate will need to create a shortcode or widget to display the feed.
 * The test will require the candidate to use Bootstrap for the styling of the page, jQuery with AJAX to fetch the results from YouTube and WordPress to create the site.
 * Extra points:
 * If the candidate has created a shortcode, please create a widget and vice-versa.
 * Display the description and title of the video and make it play in a bootstrap modal.
 * Limit the number of videos based on a parameter set in shortcode or widget.
 

# Created

* Kaplan Theme (/kaplantheme)
* Kaplan Plugin (/kaplan)
* Kaplan Widget (/kaplan/src/app/Widget)
* Kaplan Library (/src/lib/videoplaylist)
 * Kaplan Library Tests
 * This library can be moved to Vendor folder outside the site to deploy in deferent projects or CMS/Frameworks
 * If you move you should also move the local the /vendor, /tests and /lib as well as the composer and phpunit xml files

**Development Environment**

* Host machine MacBook Pro Running OS X Yosemite
* Guest machine running on Vagrant Box Ansyable with Debian 8
* PHP 5.6.14-0+deb8u1
* Koala/ Grunt to compile SASS
* Composer


**Feed Stream**

* PhP library as service layer and provider
* AJAX Global OobjectData

**Interfaces**

* Shortcode 
 * ```[video_play_list_provider videoNumber=50]```
* Widget
 * as part of the plugin in /lib

**Dependencies**

* Phpunit (Unit Testing)
* Guzzle  (RESTFULL Calls manager)
* Monolog (for logging)

* Jquery
* Bootstrap
* Font awesome

# Technicalities 

* Used regular simple Theme structure
* MVC Design Pattern for the plugin
* Singleton/Strategy Design Pattern for the Library
* PSR4 Autoload
* PSR2 Coding Style

# Goal

Create a scalable and reusable application that will not only work on Wordpress but also in any other CMS or Framework .

The service library acts as a service provider that gets the feed and deliver to any application that needs it.
This way I can connect any kind os interface to it (plugin, widget etc) and guarantee that the fed is consistency .
Also gives the oportunity to scale for diferente providers, you jas have to add a new provider following the abstract rules and you're set to go.

You can instantiate the library with the output feed like this

```php
$feed = GetPayListDataController::providerFactory('youtube', $videosNumberToDisplay);
```
The first parameter is the provider name and the second is the amount os videos to display on each call 

The plugin is Kickstarted with just
```php
app::start();
```

The output is done through views that are feed by the model that is called by the controller
In wordpress the correct way of calling and register is like this

```php
add_shortcode( 'video_play_list_provider', [new videolistController(), 'videolistControllerLoader'] );
```
You give the class/object that will handle the controllers and the method that will manage the Model and View
The parameters will be recognized by the controller and handled/served to the model

Every time you need a different output just create the desired view and call it through the appropriate controller